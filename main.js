const SERVER_PORT = 8000;

var express = require('express');
var app = express();
var server = require('http').Server( app );
var io = require('socket.io').listen( server );
var jwt = require('jsonwebtoken');
var redis = require('redis');
var ioredis = require('socket.io-redis');
var _ = require('underscore');

require('dotenv').config({path: '/var/www/html/content/.env'});
var jwtSecret = process.env.JWT_SECRET;

var sub = redis.createClient();
var auth = io.of('/auth');

var connectedUsers = {};

var pendingActions = {};

function buildCompayUnique(company){
	return 'company_' + company.unique + '_' + company.id;
}

auth.on('connection', function( socket ){
	
	socket.on('auth:request', function( authCreds ){
		
		jwt.verify(authCreds.token, jwtSecret, function( err, decoded ) {
			
			if( err || !( 'payload' in decoded ) ){
				return;
			}

			if( !( decoded.payload in connectedUsers) ){
				return;
			}

			var payload = decoded.payload;

			var companyUnique = buildCompayUnique(connectedUsers[ payload ].user.company);

			connectedUsers[ payload ].authenticated = true;

			connectedUsers[ payload ].sockets.push(socket.id);

			socket.userPayload = payload;

			socket.join( socket.userPayload );

			socket.join( companyUnique );

			if( connectedUsers[ payload ].user.channels && connectedUsers[ payload ].user.channels.length !== 0 )
			{
				connectedUsers[ payload ].user.channels.map(function(val, key)
				{
					socket.join(val);
				});
			}

			socket.join( companyUnique + '_' + connectedUsers[ payload ].user.role_name);

			var onlineUsers = findMyStaffs( companyUnique );

			socket.emit('auth:completed', onlineUsers);

			socket.broadcast.to(companyUnique).emit('socket:changed-status', onlineUsers);
		});
	});


	socket.on('user:change-status', function(status){

		if( !isNaN(status) )
		{
			var _sock = connectedUsers[ socket.userPayload ];

			if( _sock && _sock.user  ){

				var companyUnique = buildCompayUnique( _sock.user.company );
					
				connectedUsers[ socket.userPayload ].visible_status = status;

				notifyStaffStatusChanged( companyUnique );
			}
		}
	});

	socket.on('disconnect', function(){

		logout(socket);
	});

	socket.on('socket:logout', function()
	{
		logout(socket);
	});

	socket.on('redux:action', function(data){

		handleDispatch(socket, data.actionKey);

	});

});


sub.subscribe('AUTH');

sub.on('message', function(channel, message){
	message = JSON.parse(message);
	switch(message.event)
	{
		case 'authentication':
			afterAuth(message.data);
		break;

		case 'redux:dispatch-action':
			beforeReduxDispatch(message.data);
		break;

		case 'project:user-revoked':
			forceProjectReload(message.data, {reload: true});
			break;

		case 'project:user-assigned':
			forceProjectReload(message.data, {reload: false});
			break;

		case 'redux:user-refresh':
			auth.to(message.data.company).emit('refresh:user');
			break;

		case 'redux:private-message':
			sendMessage( message.data );
			break;

		case 'redux:user-updated':
			broadCastUserUpdate(message.data);
			break;

		case 'chat:stopped-typing':
		case 'chat:start-typing':
			auth.in(message.data.to).emit(message.event, { type: message.data.type,'id': message.data.id, 'writer': message.data.writer });
			break;

		case 'channel:assign-user':
			assingUsersToChannel(message.data);
			break;

		case 'channel:revoke-user':
			revokeUserFromChannel(message.data);
			break;

		case 'chat-status:changed':
			changeChatStatus(message.data);
			break;

		case 'app:logout':
			auth.in(message.data.user).emit('force:logout',message.data.redirect);
			notifyStaffStatusChanged( message.data.company );
			break;

		case 'redis:dynamic-event':
			handleDynamicEvents(message.data);
			break;
	}
});

server.listen(SERVER_PORT, function () {
    console.log('Listening to incoming client connections on port ' + SERVER_PORT);
});


//check here
function sendMessage(message){

	if( message.to && message.from )
	{
		if( message.to === message.from )
		{
			auth.to(message.to).emit('message:received', message.payload);

		}

		if( message.to !== message.from )
		{
			auth.to(message.to).emit('message:received', message.payload);
			auth.to(message.from).emit('message:received', message.payload);

		}
		
		return;
	}

	auth.to( message.to ).emit('message:received', message.payload);
}

function revokeUserFromChannel(data){

	if( data.user in connectedUsers )
	{
		var user = connectedUsers[ data.user ].user;

		var _socket = findSocketByPayload( data.user );

		if( _socket.length !== 0 )
		{
			_socket.map(function(s)
			{
				s.leave(data.channel.unique);
			});

			auth.to(data.user).emit('channel:deleted', {'id': data.channel.id});
		}
	}
}

//helper function

function afterAuth( message ) {

	var userInfo = message;
	
	jwt.verify(userInfo.token, jwtSecret, function( err, decoded ) {
		
		if( decoded && 'payload' in decoded )
		{
			var payload = decoded.payload;

			var _userInfo = {'user': userInfo, authenticated: false, sockets: [],'visible_status': 1};
			
			if( connectedUsers[ payload ] )
			{
				console.log('check',connectedUsers[ payload ].visible_status)
				_userInfo.visible_status = connectedUsers[ payload ].visible_status || 1;
			}

			connectedUsers[ payload ] = _userInfo;
		}
	})
}

//check here
function forceProjectReload(user, shouldReload){
	auth.to(user.secret).emit('reload:project',shouldReload);
} 

function beforeReduxDispatch(data){
		
	pendingActions[ data.key ] = {
	 	'eventData': data.eventData,
		 completed: false 
	}
}

function handleDispatch(socket, actionSecret){

	if( actionSecret in pendingActions )
	{
		var currentAction = pendingActions[ actionSecret ];

		var channels = currentAction.eventData;

		if( channels && Object.keys(channels).length !== 0 )
		{
			for(var i in channels)
			{
				if( channels.hasOwnProperty(i) )
				{
					var actionData = channels[ i ];

					console.log('boradcasting to ',i, 'action', actionData.action.type);

					//check here

					socket.broadcast.to( i ).emit('socket:redux-dispatched', actionData);
				}
			}
		}

		delete( pendingActions[ actionSecret ] );
	}
}

function isUserOnline(payload){
	return connectedUsers[ payload ] ? connectedUsers[ payload ].user : false;
}


function broadCastUserUpdate(data)
{
	var user = isUserOnline( data.payload );

	if( user )
	{
		auth.to( buildCompayUnique( user.company ) ).emit('user:updated', data.redux );	
	}
}

function findMyStaffs( company ){
	
	var users = auth.sockets;

	var toReturn = [];
	
	for(var i in users){
	
		if(users.hasOwnProperty(i)){

			if( users[i].userPayload && connectedUsers[ users[i].userPayload ] ){

				var user = connectedUsers[ users[i].userPayload ].user;

				var status = connectedUsers[ users[i].userPayload ].visible_status;

				if( users[i].connected )
				{				
					toReturn.push({ id : user.id, _status: ( status  === undefined ? 1 : status ) });
				}
			}
		}
	}

	return toReturn.length === 0 ? false : toReturn;
}

function notifyStaffStatusChanged(company, socket) 
{
	var onlineUsers = findMyStaffs( company );

	if( onlineUsers )
	{
		var handle = socket ? socket.broadcast : auth;

		handle.to(company).emit('socket:changed-status', onlineUsers);
	}
}

function logout(socket)
{
	socket.leave( socket.userPayload );

	var _sock = connectedUsers[ socket.userPayload ];

	if( _sock && _sock.user  ){

		var companyUnique = buildCompayUnique( _sock.user.company );

		notifyStaffStatusChanged( companyUnique, socket );
	}

	socket.disconnect();
}

function assingUsersToChannel(channelData)
{
	var users = channelData['users'];

	var activeUsers = [];

	var channel = channelData['channel'].unique;

	delete(channelData['channel'].unique)

	users.map(function(u, key){

		if( isUserOnline( u ) && channelData['channel'])
		{			
			connectedUsers[ u ].pending_channels = channel;

			activeUsers.push(u);

			auth.to(u).emit('socket:assing-channel', channelData['channel']);
		}
	});

	assignChannelToSockets(activeUsers,channel );
}

function assignChannelToSockets(users, channel)
{
	users.forEach(function(u){

		var _sock = findSocketByPayload(u);

		if(_sock.length !== 0 )
		{
			_sock.map(function(s){ s.join(channel) });
		}
	});
}

function findSocketByPayload( payload )
{
	var allSockets = io.nsps['/auth'].adapter.rooms[payload];

	var userSockets = [];

	if( allSockets && allSockets.sockets)
	{
		allSockets = allSockets.sockets;

		for(var i in allSockets)
		{
			if( allSockets.hasOwnProperty( i ) && io.of('/auth').adapter.nsp.connected[ i ].userPayload == payload )
			{
				userSockets.push(io.of('/auth').adapter.nsp.connected[i]);
			}
		}
	}

	return userSockets;
}

function changeChatStatus(message)
{
	if( isUserOnline( message.to ) )
	{
		auth.to( message.to ).emit('chat:status-changed', message.payload);
	}
}

//to do
function handleDynamicEvents(data)
{
	if( Array.isArray(data.to)  && data.to.length !== 0 && data._event )
	{
		for( var i = 0; i<= data.to.length; i++)
		{
			auth.to(data.to[i]).emit(data._event, data.payload);
		}
	}
	else
	{
		for(var i in data.to)
		{
			if( data.to.hasOwnProperty(i) ){
				auth.to( i ).emit(data._event, data.to[i]);
			}
		}
	}
}