const SERVER_PORT = 8585;

var express = require('express');
var app = express();
var server = require('http').Server( app );
var io = require('socket.io').listen( server );
var jwt = require('jsonwebtoken');
var redis = require('redis');
var ioredis = require('socket.io-redis');
var _ = require('underscore');

var sub = redis.createClient();

sub.subscribe('LOCATION');

sub.on('message', function(channel, message){
    console.log(channel, message)
});

var guest = io.of('/guest');

guest.on('connection', function(socket){
    console.log('connected');
    socket.on('location:data', function(data){
        console.log('Location Regular', data);
    });

    socket.on('location:accurate', function(data){
        console.log('location accurate', data);
    });
});

server.listen(SERVER_PORT, function () {
    console.log('Listening to incoming client connections on port ' + SERVER_PORT);
});

	// socket.on('send_message', function(message){
	// 	var user = connectedUsers[ socket.userPayload ];

	// 	if( parseInt( message.type) === 0 ){
	// 		socket.broadcast.to(message.token).emit('message_received', message);
	// 		return;
	// 	}

	// 	if(user.sockets.length > 1){
	// 		socket.broadcast.to( socket.userPayload ).emit('message_received', message);
	// 	}

	// 	var receiverSocekt = connectedUsers[ message.token ];

	// 	if(receiverSocekt && receiverSocekt.authenticated === true)
	// 	{
	// 		message.id = message.chat.author_id;
	// 		socket.broadcast.to(message.token).emit('message_received',message);
	// 	}

	// });

	// socket.on('user_typing', function(params){
	// 	socket.broadcast.to( params.to ).emit('someone_typing', params.from);
	// });

	// socket.on('stop_typing', function(){
	// 	socket.broadcast.to(params.to).emit('stop_typing', params.from );
	// });

	// socket.on('subscribe_channel', function(channels){
	// 	socket.subscribedChannel = channels;
	// 	channels.forEach(function(c){
	// 		socket.join(c);
	// 	});
	// });